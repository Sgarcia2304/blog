﻿## Donderdag 12-10-2017

Vandaag hebben we onze tentamen Design Theorie, maar helaas moet ik eerst in de ochtend van 8:30 tot 12:00 werken. Ik heb gelukkig nog tijd zat om thuis te douchen, wat te eten en nog even alles doornemen. Maar het meest belangrijke van alles... uitrusten. Dat heb ik echt even nodig. Ik ga er deze keer wel voor zorgen dat ik 30 min van te voren op school ben. Ik moet ook niet vergeten om me ID en spiekbrief mee te nemen.

Dit is dus mijn spiekbriefje. Toen ik begon met leren had ik zes kantjes volgeschreven met belangrijke informatie. Dus ik had gekke paniek van hoe krijg ik dat ooit op één kantje. Ik ben toen extra klein gaan schrijven en uiteindelijk had ik alles erop geschreven en toen bleek dat ik nog meer dan genoeg ruimte had. Dus ik dacht dan maak ik er toch een paar kleine visuals op.

![enter image description here](https://lh3.googleusercontent.com/kHnjN6WrvXMmQIKgruTJBOadvfzLSvgBL-HYizyxHkWPNB8IkUCkgwIkZbRCD975vqan0-_XdBqQ=s1000 "20171023_201634.jpg")

Oké ik ben op school heb inderdaad iets van 30 min de tijd om even snel wat te drinken, eten en om even verdwaald te raken in deze school.

Nou tentamen gemaakt!!! En het is echt beter gegaan dan ik ooit had mogen hopen. Ik was binnen 20 min klaar, alleen ik ben nog blijven zitten en heb heel die tentamen nog doorgelezen, goed gekeken of ik de juiste antwoord bij de juiste vraag heb aangekruist. en daarna had ik mijn tentamen ingeleverd. 

Dus nu op naar huis en een dutje doen want ik kan echt niet meer, ik ben blij dat we morgen geen school hebben. Maar ook dat we niet gaan vergaderen als team, want tja op dit moment valt er nog helemaal niks om over te vergaderen of wat dan ook. We krijgen pas onze derde iteratie na de vakantie dus tot die tijd kan ik een beetje genieten en tot rust komen.





